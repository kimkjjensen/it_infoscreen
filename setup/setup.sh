#!/bin/bash

COLOR='\033[0;32m' # dejlig gr�n
ENDCOLOR='\033[1;37m' # hvid
# ${COLOR} f�r �nsket tekst for at anvende p� resten af linjen
# Progress-beskeder echoes. -e i echo betyder at den skal tage specialtegn, eks. \n som linjeskift.
echo -e "${COLOR}Flexterminal-setup starter\nOpdaterer system${ENDCOLOR}"
# sleep i 1 sekund for synlighed af progress
sleep 1s
# Opdater system
sudo apt update
sudo apt full-upgrade -y

# Installation af Unclutter til at usynligg�re musemark�ren, Chromium-browseren, og desktop-manager
echo -e "${COLOR}Installerer desktop, unclutter, og chromium-browser${ENDCOLOR}"
sleep 1s
sudo apt install --no-install-recommends xserver-xorg xinit -y
sudo apt install raspberrypi-ui-mods -y
sudo apt install unclutter chromium-browser -y

# Oprettelse af ~/.config/lxsession/LXDE-pi/ til brug til at starte unclutter og Chromium
echo -e "${COLOR}Opretter ~/.config/lxsession/LXDE-pi/ til autostart hvis mappen ikke eksisterer${ENDCOLOR}"
sleep 1s
# Argument -p for at oprette hele stien (~/.config/lxsession/ mangler som standard)
mkdir -p ~/.config/lxsession/LXDE-pi/

# Inds�tning af programmer der skal starte i ~/.config/lxsession/LXDE-pi/autostart-filen med konfiguration ...
# VHA >> (output redirection), som her bruges til at inds�tte output fra echo-kommandoen i en fil i stedet for terminalen
echo -e "${COLOR}Erstatter indholdet af autostart-filen til autostart af unclutter, og Chromium, og at sl� screensaver fra${ENDCOLOR}"
sleep 1s
echo -e "@unclutter -display:0 -noevents -grab\n@xset s off\n@xset -dpms\n@chromium-browser --kiosk --incognito http://10.171.215.143/printer/touch.php" > ~/.config/lxsession/LXDE-pi/autostart

# NB! sudo bash -c '$commands' bruges til at g�re det muligt at bruge output redirection (>>) til at inds�tte en linje i en fil som kr�ver ...
# flere permissions end den p�g�ldende bruger har
sleep 1s
# Conditional for ikke at inds�tte samme linje flere gange
if grep -q 'lcd_rotate=2' '/boot/config.txt';
    then
        echo -e "${COLOR}lcd_rotate=2 eksisterer allerede i /boot/config.txt${ENDCOLOR}"
    else
        echo -e "${COLOR}Inds�tter 180 graders rotation af display for korrekt visning${ENDCOLOR}"
        sudo bash -c 'echo "lcd_rotate=2" >> /boot/config.txt'
fi

echo -e "${COLOR}Autologin til brugeren pi${ENDCOLOR}"
sleep 1s
# Sed bruges her til at erstatte linjen "#autologin-user=" med "autologin-user=pi". 
# -i betyder at noget tekst skal inds�ttes. s/ betyder find m�nster/tekst, efter n�ste / er det der skal erstattes med, /g definerer at det fundne skal erstattes.
sudo sed -i 's/#autologin-user=/autologin-user=pi/g' /etc/lightdm/lightdm.conf
sleep 1s
echo -e "${COLOR}Genstarter${ENDCOLOR}"
# Afslut med at genstarte
sudo reboot
